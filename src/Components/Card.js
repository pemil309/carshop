const Card = ({car}) => {
    return (
        <tr>
            <td>{car.id}</td>
            <td>{car.car.make}</td>
            <td>{car.car.model}</td>
            <td>{car.car.type}</td>
            <td>{car.car.vin}</td>
            <td>{car.car.plate}</td>
            <td>{car.car.year}</td>
            <td>{car.car.fuel}</td>
            <td style={
                {
                    display: "flex",
                    justifyContent: "center"
                }
            }>
                {car.sold && <div className="check"></div>}
                {!car.sold && <div className="close"></div>}
            </td>
            <td >
                <button >Edit</button>
                <button >Delete</button>
            </td>
        </tr>
    )
}
export default Card