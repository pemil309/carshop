import axios from "axios"
import {useState, useEffect, useRef} from "react"
import Card from "../src/Components/Card.js"
import postData from "../src/post"

const App = () => {
    const [cars, setCars] = useState([])
    const [filtered, setFiltered] = useState([])
    const [updated, setUpdated] = useState(-1)
    const fetchData = async () => {
        try {
            const carData = await axios.get("http://localhost:8000/cars")
            const data = Object.keys(carData.data.data).map(
                (car) => carData.data.data[car]
            )
            data.sort((a, b) => a.id - b.id)
            setCars(data)
            setFiltered(data)
        } catch (err) {
            console.error(err)
        }
    }

    const handleSearch = (e) => {
        const searchString = e.target.value
        if (searchString === "") {
            setFiltered(cars)
            return
        }

        const filteredCars = cars.filter((car) => {
            return car.car.make.toLowerCase().includes(searchString)
        })
        setFiltered(filteredCars)
    }

    function EditList() {
        function handleUpdate() {

        }

        return(
            <tr>
                <td> <input type="number" name="id" placeholder="Enter ID" ref={idRef}/> </td>
                <td> <input type="text" name="brand" placeholder="Enter brand" ref={brandRef}/> </td>
                <td> <input type="text" name="model" placeholder="Enter model" ref={modelRef}/> </td>
                <td> <input type="text" name="type" placeholder="Enter type" ref={typeRef}/> </td>
                <td> <input type="text" name="VIN" placeholder="Enter VIN" ref={vinRef}/> </td>
                <td> <input type="text" name="plate" placeholder="Enter plate" ref={plateRef}/> </td>
                <td> <input type="number" name="year" placeholder="Enter year" ref={yearRef}/> </td>
                <td> <input type="text" name="fuel" placeholder="Enter fuel" ref={fuelRef}/> </td>
                <td> <input type="text" name="status" placeholder="Enter status" ref={statusRef}/> </td>
                <td> <button type="submit"> Add</button> </td>
            </tr>
        )
    }

    function AddCar({setCars}) {
        // const idRef = useRef();
        const brandRef = useRef();
        const modelRef = useRef();
        const typeRef = useRef();
        const vinRef = useRef();
        const plateRef = useRef();
        const yearRef = useRef();
        const fuelRef = useRef();


        function handleSubmit (e) {
            e.preventDefault()
            let id = 11;
            const brand = e.target.elements.brand.value
            const model = e.target.elements.model.value
            const type = e.target.elements.type.value
            const VIN = e.target.elements.VIN.value
            const plate = e.target.elements.plate.value
            const year = e.target.elements.year.value
            const fuel = e.target.elements.fuel.value
            const status = true;

            const newList = {
                id,
                car : {
                    brand,
                    model,
                    type,
                    VIN,
                    plate,
                    year,
                    fuel
                },
                status,
            }
            setCars((prevList) => {
                return prevList.push(newList)
            })
            id += 1;
        }

        return (
            <form onSubmit={handleSubmit}>
                {/*<input className="addInput" type="number" name="id" placeholder="Enter ID" ref={idRef}/>*/}
                <input className="addInput" type="text" name="brand" placeholder="Enter brand" ref={brandRef}/>
                <input className="addInput" type="text" name="model" placeholder="Enter model" ref={modelRef}/>
                <input className="addInput" type="text" name="type" placeholder="Enter type" ref={typeRef}/>
                <input className="addInput" type="text" name="VIN" placeholder="Enter VIN" ref={vinRef}/>
                <input className="addInput" type="text" name="plate" placeholder="Enter plate" ref={plateRef}/>
                <input className="addInput" type="number" name="year" placeholder="Enter year" ref={yearRef}/>
                <input className="addInput" type="text" name="fuel" placeholder="Enter fuel" ref={fuelRef}/>
                <button className="addInput" type="submit"> Add</button>
            </form>
        )
    }

    useEffect(() => {
        fetchData()
    }, [])

    // postData('http://localhost:8000/cars', { data: 42 }).then((data) => {
    //     console.log(data)
    // })


    return (
        <div className="App">
            <div>
                <input onKeyUp={handleSearch} type="text" id="searchBar" placeholder="Search car by brand..."/>
            </div>
            <div>
                <table rules="all">
                    <thead>
                    <tr>
                        <th> ID</th>
                        <th> Brand</th>
                        <th> Model</th>
                        <th> Body type</th>
                        <th> VIN</th>
                        <th> Plate</th>
                        <th> Manufacturing year</th>
                        <th> Fuel</th>
                        <th> Status</th>
                    </tr>
                    </thead>
                    <tbody>
                    {filtered.map((car) => (<Card key={car.id} car={car}/>))}
                    </tbody>
                </table>
            </div>
            <br/>
            <div>
                <AddCar setCars={setCars}/>
            </div>
        </div>
    )
}
export default App